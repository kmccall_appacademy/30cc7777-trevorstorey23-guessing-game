# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def guessing_game
  guesses = 0
  guess = nil
  target_number = rand(1..100)
  until guess == target_number
    puts "guess a number between 1 and 100"
    guess = gets.chomp.to_i
    if guess < target_number
      puts "#{guess} is too low"
    elsif guess > target_number
      puts "#{guess} is too high"
    end
    guesses += 1
    puts guess
  end
  puts guess
  puts "You guessed it!\n#{guess} is the number!"
  return "It took you #{guesses} guesses"
end

def file_shuffler
  puts "What's the file name?"
  file_name = gets.chomp

  contents = File.readlines(file_name)
  new_file = contents.shuffle
  
  File.open("#{file_name}-shuffled.txt", "w") do |f|
    new_file.each { |line| f.puts line }
  end

end
